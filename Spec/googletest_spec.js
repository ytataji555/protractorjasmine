"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
const googlelib_1 = require("../lib/googlelib");
protractor_1.browser.ignoreSynchronization;
describe('Automation testing on angular js application', function () {
    let gl = new googlelib_1.googlelib();
    // browser.ignoreSynchronization=true
    it('Verification of title of the angulario application home page', function () {
        protractor_1.browser.get('https://angular.io');
        expect(protractor_1.browser.getTitle()).toEqual('Angular');
    });
    it('Should be able to enter text in searchbar ', function () {
        protractor_1.browser.get('https://angular.io')
            .then(function () {
            googlelib_1.googlelib.entertext("tataji");
        });
    });
});
//# sourceMappingURL=googletest_spec.js.map