var HtmlReporter = require('protractor-beautiful-reporter');
// let displayDate = new Date().toLocaleDateString();
// let re = /\-/gi;
// let Date_actual=displayDate.replace(re,"")

// let now = new Date().toLocaleTimeString(); 
// let re1 = /\:/gi;
// let now_actual=now.replace(re1,"")
// let final_date_time=Date_actual+'-'+now_actual

exports.config={
    
    seleniumAddress:'http://localhost:4444/wd/hub',
    specs:['C:\\Users\\tyerubandhi\\Documents\\protractordemo\\Spec\\beertest_spec.js'],
   capabilities:
     {
        browserName:'chrome' 
     },
    onPrepare: function() {
        
     //Add a screenshot reporter and store screenshots to `/tmp/screenshots`:
        jasmine.getEnv().addReporter(new HtmlReporter({
           baseDirectory: 'FinalReport/screenshots',
          //docName: 'Report'+final_date_time+'.html',
           docName: 'Report.html',
           docTitle: 'EgainAutomationReport',
           preserveDirectory: false,
           consolidate: true,
          consolidateAll: true,
           showColors: false,
        }).getJasmine2Reporter());
      } 

};
