"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var googlelib_1 = require("../lib/googlelib");
protractor_1.browser.ignoreSynchronization;
describe('My app', function () {
    var gl = new googlelib_1.googlelib();
    protractor_1.browser.ignoreSynchronization;
    console.log('Starting test suite "GUEST"');
    it('Should automatically redirect', function () {
        protractor_1.browser.get('http://www.google.co.in')
            .then(function () {
            console.log('Start test 1: automatic redirection of index');
            googlelib_1.googlelib.entertext("tataji");
            expect(protractor_1.browser.getTitle()).toEqual('Google');
            //expect(browser.getLocationAbsUrl()).toMatch("/test");
        });
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ29vZ2xldGVzdF9zcGVjLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vU3BlYy9nb29nbGV0ZXN0X3NwZWMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSx5Q0FBbUM7QUFDbkMsOENBQTZDO0FBRTdDLG9CQUFPLENBQUMscUJBQXFCLENBQUE7QUFDN0IsUUFBUSxDQUFDLFFBQVEsRUFBRTtJQUNuQixJQUFJLEVBQUUsR0FBQyxJQUFJLHFCQUFTLEVBQUUsQ0FBQztJQUNuQixvQkFBTyxDQUFDLHFCQUFxQixDQUFBO0lBQzdCLE9BQU8sQ0FBQyxHQUFHLENBQUMsNkJBQTZCLENBQUMsQ0FBQztJQUUzQyxFQUFFLENBQUMsK0JBQStCLEVBQUU7UUFDaEMsb0JBQU8sQ0FBQyxHQUFHLENBQUMseUJBQXlCLENBQUM7YUFDckMsSUFBSSxDQUFDO1lBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO1lBQzVELHFCQUFTLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFBO1lBQzVDLE1BQU0sQ0FBQyxvQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFBO1lBQzdCLHVEQUF1RDtRQUMvRCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQyxDQUFDLENBQUMifQ==